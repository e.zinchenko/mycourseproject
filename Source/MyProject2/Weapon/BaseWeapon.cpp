// Fill out your copyright notice in the Description page of Project Settings.


#include "BaseWeapon.h"
#include "MessageDialog.h"
#include "GameFramework/Character.h"
#include "ImpactEffectComponent.h"
#include "TimerManager.h"

ABaseWeapon::ABaseWeapon()
{
	GetStaticMeshComponent()->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	GetStaticMeshComponent()->SetMobility(EComponentMobility::Movable);

	ImpactEffectComp = CreateDefaultSubobject<UImpactEffectComponent>(TEXT("ImpactEffectComponent"));
}

void ABaseWeapon::StartFire()
{
	//Fire();
	
	GetWorldTimerManager().SetTimer(AutoFire_TH, this, &ABaseWeapon::Fire, 60 / FireRate, bAutoFire, 0);
}

void ABaseWeapon::StopFire()
{
	GetWorldTimerManager().ClearTimer(AutoFire_TH);
}

void ABaseWeapon::Fire()
{
	if (FireMontage)
	{
		if (WeaponOwner)
		{
			WeaponOwner->PlayAnimMontage(FireMontage);
		}
		else
		{
			UE_LOG(LogTemp, Warning, TEXT("No Owner Found"));
		}
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("No Fire Montage Found"));
	}
}

FTransform ABaseWeapon::GetMuzzleTransform()
{
	if (MuzzleSocketName.IsNone())
	{
		FMessageDialog::Debugf(NSLOCTEXT("Error", "Key", "MuzzleSocketName Not initialized!"));
		UE_LOG(LogTemp, Warning, TEXT("Some msg"));
		return FTransform::Identity;
	}

	return 	GetStaticMeshComponent()->GetSocketTransform(MuzzleSocketName);
}

void ABaseWeapon::BeginPlay()
{
	Super::BeginPlay();

	if (GetOwner())
	{
		WeaponOwner = Cast<ACharacter>(GetOwner());
		if (!WeaponOwner)
		{
			UE_LOG(LogTemp, Warning, TEXT("Owner is not Character"));
		}
	}
}
