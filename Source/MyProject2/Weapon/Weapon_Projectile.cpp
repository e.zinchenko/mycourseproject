// Fill out your copyright notice in the Description page of Project Settings.


#include "Weapon_Projectile.h"
#include "BaseProjectile.h"
#include "ImpactEffectComponent.h"

void AWeapon_Projectile::Fire()
{
	Super::Fire();

	if (ProjectileClass)
	{
		FTransform TmpTransform = GetMuzzleTransform();

		ABaseProjectile* TmpProj = Cast<ABaseProjectile>(GetWorld()->SpawnActor(ProjectileClass, &TmpTransform));

		TmpProj->OnHitFire.BindDynamic(ImpactEffectComp, &UImpactEffectComponent::SpawnImpactEffect);
	}

}
