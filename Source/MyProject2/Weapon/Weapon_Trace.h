// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Weapon/BaseWeapon.h"
#include "Weapon_Trace.generated.h"

/**
 * 
 */
UCLASS(abstract)
class MYPROJECT2_API AWeapon_Trace : public ABaseWeapon
{
	GENERATED_BODY()
	
public:
	virtual void Fire() override;

	UPROPERTY(EditDefaultsOnly, Category = "Trace")
		float TraceLength = 200;
	UPROPERTY(EditDefaultsOnly, Category = "Trace")
		float SphereRadius = 50;

	UPROPERTY(EditDefaultsOnly)
		class UParticleSystem* Beam;
};
