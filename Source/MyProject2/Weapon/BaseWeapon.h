// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/StaticMeshActor.h"
#include "BaseWeapon.generated.h"

/**
 * 
 */
UCLASS(abstract)
class MYPROJECT2_API ABaseWeapon : public AStaticMeshActor
{
	GENERATED_BODY()

public:

	ABaseWeapon();

	virtual void StartFire();

	virtual void StopFire();

protected:

	UPROPERTY(VisibleAnywhere)
		class UImpactEffectComponent* ImpactEffectComp;

	UPROPERTY(EditDefaultsOnly, Category = "Muzzle")
		FName MuzzleSocketName;

	//Shots per minute
	UPROPERTY(EditDefaultsOnly, Category = "Autofire", meta = (EditCondition = "bAutoFire"))
		float FireRate = 250;
	UPROPERTY(EditDefaultsOnly, Category = "Autofire")
		bool bAutoFire = false;


	UPROPERTY()
		class ACharacter* WeaponOwner;

	FTimerHandle AutoFire_TH;

public:

	UPROPERTY(EditDefaultsOnly, Category = "Montages")
		class UAnimMontage* FireMontage;

	UFUNCTION()
	virtual void Fire();

	FTransform GetMuzzleTransform();
	
	virtual void BeginPlay() override;

};
