// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "MyCharacter.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FMyDelegate, bool, bMyBool);

class UCameraComponent;
class USpringArmComponent;

UCLASS()
class MYPROJECT2_API AMyCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	AMyCharacter();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	void MoveForward(float Scale);
	void MoveRight(float Scale);

	void StartFire();
	void StopFire();

	class ABaseWeapon* SpawnWeapon();
	void AttachWeapon(FName SocketName);

	UFUNCTION()
	void CharacterDied();

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		class UVitalityComponnet* VitalityComp;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	UCameraComponent* CameraComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	USpringArmComponent* SpringArmComponent;

	UPROPERTY(EditDefaultsOnly, Category = "BaseWeapon")
		TSubclassOf<class ABaseWeapon> WeaponToSpawn_Class;
	UPROPERTY(EditDefaultsOnly, Category = "BaseWeapon")
		FName WeaponSocket;

	UPROPERTY()
		class ABaseWeapon* CurrentWeapon;
// 	UPROPERTY()
// 		class UAnimMontage* FireMontage;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	void RotateToMouse();

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

};
