// Fill out your copyright notice in the Description page of Project Settings.


#include "VitalityComponnet.h"
#include "MyTestFolder/Core/MyGameInstance.h"

// Sets default values for this component's properties
UVitalityComponnet::UVitalityComponnet()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UVitalityComponnet::BeginPlay()
{
	Super::BeginPlay();

	if (GetOwner())
	{
		GetOwner()->OnTakeAnyDamage.AddDynamic(this, &UVitalityComponnet::DamageHadle);
	}

}


void UVitalityComponnet::DamageHadle(AActor* DamagedActor, float Damage, const class UDamageType* DamageType, class AController* InstigatedBy, AActor* DamageCauser)
{

	if (!IsAlive())
	{
		UE_LOG(LogTemp, Warning, TEXT("Already Dead"));
		return;
	}

	UE_LOG(LogTemp, Warning, TEXT("%s was damaged for %s"), *DamagedActor->GetName(), *FString::SanitizeFloat(Damage));

	CurrentHealth -= Damage;

	OnHealthChanged.Broadcast();

	if (!IsAlive())
	{
		//Death
		OnOwnerDied.Broadcast();
		UE_LOG(LogTemp, Warning, TEXT("OwnerDied"));

		UMyGameInstance* MyInstance = Cast<UMyGameInstance>(GetWorld()->GetGameInstance());
		if (MyInstance)
		{
			MyInstance->AddScore(Score);
		}
	}
}

// Called every frame
void UVitalityComponnet::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

bool UVitalityComponnet::IsAlive()
{
	return CurrentHealth > 0;
}

