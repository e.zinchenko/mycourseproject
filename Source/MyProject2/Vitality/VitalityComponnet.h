// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "VitalityComponnet.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FNoParamDelegate);

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class MYPROJECT2_API UVitalityComponnet : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UVitalityComponnet();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

	UFUNCTION()
		void DamageHadle(AActor* DamagedActor, float Damage, const class UDamageType* DamageType, class AController* InstigatedBy, AActor* DamageCauser);

	UPROPERTY(EditInstanceOnly, BlueprintReadOnly, Category = "Health")
		float MaxHealth = 100;
	UPROPERTY(BlueprintReadOnly, Category = "Health")
		float CurrentHealth = MaxHealth;

	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly, Category = "Score")
		int Score = 100;

	UPROPERTY(BlueprintAssignable, Category = "Delegates")
	FNoParamDelegate OnHealthChanged;


public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	UPROPERTY(BlueprintAssignable, Category = "Delegates")
	FNoParamDelegate OnOwnerDied;

	UFUNCTION(BlueprintCallable)
		bool IsAlive();
		
};
